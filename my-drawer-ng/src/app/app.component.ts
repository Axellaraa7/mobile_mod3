import { Component, OnInit } from '@angular/core'
import { NavigationEnd, Router } from '@angular/router'
import { RouterExtensions } from '@nativescript/angular'
import { DrawerTransitionBase, RadSideDrawer, SlideInOnTopTransition } from 'nativescript-ui-sidedrawer'
import { filter } from 'rxjs/operators'
import { Message } from "nativescript-plugin-firebase";
import { Application } from '@nativescript/core'
import * as AppSettings from '@nativescript/core/application-settings';
const firebase = require("nativescript-plugin-firebase");
import * as Toast from "nativescript-toasts";

@Component({
  selector: 'ns-app',
  templateUrl: 'app.component.html',
})
export class AppComponent implements OnInit {
  private _activatedUrl: string;
  private _sideDrawerTransition: DrawerTransitionBase;
  private user: string;
  private email: string

  constructor(private router: Router, private routerExtensions: RouterExtensions) {
    // Use the component constructor to inject services.
  }

  ngOnInit(): void {
    this._activatedUrl = '/home'
    this._sideDrawerTransition = new SlideInOnTopTransition()

    this.router.events
      .pipe(filter((event: any) => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => (this._activatedUrl = event.urlAfterRedirects))
    
    if (AppSettings.getString("user") == undefined){
      AppSettings.setString("user","user");
      this.user= AppSettings.getString("user");
    }else{
      this.user= AppSettings.getString("user");
      console.log("Usuario: "+ this.user);
    }

    if(AppSettings.getString("email") == undefined){
      AppSettings.setString("email","email@email.com");
      this.user= AppSettings.getString("email");
    }else{
      this.user= AppSettings.getString("email");
      console.log("Email: "+ this.email);
    }

    firebase.init({onMessageReceiveCallback : (message:Message) => {
      console.log(`titulo: ${message.title}`);
      console.log(`cuerpo: ${message.body}`);
      console.log(`data: ${JSON.stringify(message.data)}`);
      Toast.show({text: "Notification: "+ message.title, duration: Toast.DURATION.SHORT});
    },
    onPushTokenReceivedCallback: (token) => console.log("Firebase push token: "+ token)
    }).then( () => console.log("firebase.init done"), (error) => console.log("firebase.init error: "+error));
  }

  get sideDrawerTransition(): DrawerTransitionBase {
    this.user = AppSettings.getString("user");
    this.email = AppSettings.getString("email");
    return this._sideDrawerTransition
  }

  isComponentSelected(url: string): boolean {
    return this._activatedUrl === url
  }

  onNavItemTap(navItemRoute: string): void {
    this.routerExtensions.navigate([navItemRoute], {
      transition: {
        name: 'fade',
      },
    })

    const sideDrawer = <RadSideDrawer>Application.getRootView()
    sideDrawer.closeDrawer()
  }
}
