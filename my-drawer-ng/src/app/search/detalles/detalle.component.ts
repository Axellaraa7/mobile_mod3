import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import { NoticiasService } from "../../domain/noticias.service";
import { RouterExtensions } from '@nativescript/angular';

@Component({
  selector: 'ns-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})
export class DetalleComponent implements OnInit {

  constructor(public noticias: NoticiasService, private routerExtensions: RouterExtensions) {
      // Use the component constructor to inject providers.
  }

  ngOnInit(): void {
  }

  onDrawerButtonTap(): void {
      const sideDrawer = <RadSideDrawer>Application.getRootView();
      sideDrawer.showDrawer();
  }

  goBack(): void {
    this.routerExtensions.back();
  }

  

}