import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core'
import { NativeScriptCommonModule } from '@nativescript/angular'
import { NativeScriptFormsModule } from '@nativescript/angular/forms'

import { SearchRoutingModule } from './search-routing.module'
import { SearchComponent } from './search.component'
import { SearchFormComponent } from './search-form.component'

import { NoticiasService } from '../domain/noticias.service'
import { MinLenDirective } from './minLen.directive';
import { DetalleComponent } from './detalle/detalle.component';
import { FavoritosComponent } from './favoritos/favoritos.component';

@NgModule({
  imports: [NativeScriptCommonModule, SearchRoutingModule],
  declarations: [SearchComponent, SearchFormComponent, MinLenDirective, DetalleComponent, FavoritosComponent],
  //providers: [NoticiasService ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class SearchModule {}
