import { Injectable } from "@angular/core";
import { getJSON, request } from "@nativescript/core/http";
const sqlite= require("nativescript-sqlite")

@Injectable()
export class NoticiasService{
	api: string= "https://303c446f.ngrok.io"

	constructor(){
		this.getDb((db) => {
			console.dir(db);
			db.each("select * from logs", (err,fila) => console.log("fila: ", fila), (err, totales) => console.log("Filas totales: ", totales));
		}, () => console.log("error on getDB"));
	}
	
	getDb(fnOk,  fnError){
		return new sqlite("mi_db_logs", (err,db) => {
		  if(err){
		  	console.error("Error al abrir la db",err);
		  }else{
		  	console.log("Esta abierta la db", db.isOpen() ? "Si":"No");
		  	db.execSQL("CREATE TABLE IF NOT EXISTS logs(id INTEGER PRIMARY KEY AUTOINCREMENT, texto TEXT").then((id) =>{
		  		console.log("CREATE TABLE OK");
		  		fnOk(db);
		  	},(error) => {
		  		console.log("CREATE TABLE ERROR", error);
		  		fnError(error);
		  	});
		  }
		});
	}
	agregar(s: string){
		return request({
			url: this.api + "/favs",
			method: "POST",
			headers: { "Content-type": "application/json"},
			content: JSON.stringify({
				nuevo: s
			})
		});
	}

	favs(){
		return getJSON(this.api+"/favs");
	}
	buscar(s: string){
		return getJSON(this.api + "/get?q="+s);
	}
}