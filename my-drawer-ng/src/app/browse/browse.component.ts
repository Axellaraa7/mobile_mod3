import { Component, OnInit, ViewChild, ElementRef } from '@angular/core'
import { RadSideDrawer } from 'nativescript-ui-sidedrawer'
import { Application } from '@nativescript/core'
import { registerElement } from "@nativescript/angular/element-registry";

registerElement("MapView", () => require("nativescript-google-maps-sdk").MapView);

@Component({
  selector: 'Browse',
  templateUrl: './browse.component.html',
})
export class BrowseComponent implements OnInit {
  @ViewChild("MapView") MapView: ElementRef;
  constructor() {
    // Use the component constructor to inject providers.
  }

  ngOnInit(): void {
    // Init your component properties here.
  }

  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>Application.getRootView()
    sideDrawer.showDrawer()
  }

  onMapReady(event): void{
    console.log("Map Ready");
    var gmaps = require("nativescript-google-maps-sdk");

    function onMapReady(args){
      var mapView = args.object;
      var marker = new gmaps.Marker();
      marker.position = gmaps.Position.positionFromLatLng(-34.6037,-58.3817);
      marker.title="Buenos Aires";
      marker.sinppet="Argentina"
      marker.userData={ index : 1};
      mapView.addMarker(marker);
    }
  }
}
