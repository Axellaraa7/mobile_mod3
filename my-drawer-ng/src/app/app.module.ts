import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core'
import { NativeScriptModule } from '@nativescript/angular'
import { NativeScriptUISideDrawerModule } from 'nativescript-ui-sidedrawer/angular'

import { EffectsModule } from '@ngrx/effects';
import { ActionReducerMap, StoreModule as NgRxStoreModule } from '@ngrx/store';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component'

import { initializeNoticias, NoticiasEffects, NoticiasState, reducerNoticias, Noticia } from './domain/noticias-state.model';

import { NoticiasService } from './domain/noticias.service'
import { DetalleComponent } from './search/detalles/detalle.component';

export interface AppState{
	noticias: NoticiasState;
}

const reducers: ActionReducerMap<AppState> = {
	noticias: reducerNoticias
};

const reducersInitialState = {
	noticias: initializeNoticias()
};

@NgModule({
  bootstrap: [AppComponent],
  imports: [AppRoutingModule, NativeScriptModule, NativeScriptUISideDrawerModule, NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState}), EffectsModule.forRoot([NoticiasEffects])],
  declarations: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [ NoticiasService, Noticia, DetalleComponent, FavoritosComponent ]
})
export class AppModule {}
