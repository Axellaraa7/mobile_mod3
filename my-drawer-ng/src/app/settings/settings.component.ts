import { Component, OnInit } from '@angular/core';
import * as Toast from 'nativescript-toasts';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
import { Application } from '@nativescript/core';
import { PromptOptions, PromptResult } from '@nativescript/core/ui/dialogs';
import * as dialogs from '@nativescript/core/ui/dialogs';
import * as app from '@nativescript/core/application';
import * as AppSettings from '@nativescript/core/application-settings'

@Component({
  selector: 'Settings',
  templateUrl: './settings.component.html',
  moduleId: module.id
})
export class SettingsComponent implements OnInit {
  private user: string;
  private email: string;
  constructor() {
    if (AppSettings.getString("user") == undefined) {
      AppSettings.setString("user", "user");
    }
  }

  doLater(fn){ setTimeout(fn,1000); }

  ngOnInit(): void {
    /*this.doLater(() =>
      dialogs.action("Mensaje", "Cancelar!", ["Opcion1", "Opcion2"]).then((result) => {
        console.log("Resultado "+result);
        if(result==="Opcion1"){
          this.doLater(() =>
            dialogs.alert({
              tutle: "Titulo1",
              message: "mje1",
              okButtonText: "btn1"
            }).then(() => 
              console.log("Cerrado1")));
        }else if (result === "Opcion2"){
          this.doLater(() => 
            dialogs.alert({
              title: "Titulo 2",
              message: "mje 2",
              okButtonText: "btn 2"
            }).then(() =>
            console.log("Cerrdo2")));
        }
      }));*/
    const toastOption: Toast.ToastOptions = {text: "Hello World", duration: Toast.DURATION.SHORT};
    this.doLater(() => Toast.show(toastOption));
  }

  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>Application.getRootView()
    sideDrawer.showDrawer()
  }

  onUserButton() {
    let options: PromptOptions = {
      title: "Nombre de usuario",
      inputType: dialogs.inputType.text,
      defaultText: "user",
      okButtonText: "Guardar",
      cancelButtonText: "Cancelar",
      cancelable: true
    }

    dialogs.prompt(options).then((result: PromptResult) => {
      if (result.result === true) {
        if(result.text.length > 0) {
          AppSettings.setString("user", result.text);
          console.log("user: " + AppSettings.getString("user"));
          this.user = AppSettings.getString("user");
        }
      }
    })
  }

  onEmailButton() {
    let options: PromptOptions = {
      title: "Correo de usuario",
      inputType: dialogs.inputType.text,
      defaultText: "email@mail.com",
      okButtonText: "Guardar",
      cancelButtonText: "Cancelar",
      cancelable: true
    }

    dialogs.prompt(options).then((result: PromptResult) => {
      if (result.result === true) {
        if(result.text.length > 0) {
          AppSettings.setString("email", result.text);
          console.log("email: " + AppSettings.getString("email"));
          this.email = AppSettings.getString("email");
        }
      }
    })
  }
}
