module.exports = function(config) {
	config.set({
		basePath: '',

		frameworks: ['jasmine'],

		files: [
		'src/**/*.spec.js'
		],

		exclude: [
		],

		preprocessors:{
		},

		reporters:['progress', 'junit'],

		junitReporter:{
			outputDir: process.env.JUNIT_REPORT_PATH||'',
			outputFile: process.env.JUNIT_REPORT_NAME||undefined,
			suite:'',
			useBrowserName: false,
			nameFormatter: undefined,
			classNameFormatter: undefined,
			properties: {}
		}

		port: 9876,

		colors:true,

		logLever: config.LOG_INFO,

		autoWatch: true,

		browsers: [],

		customLaunchers:{
			android:{
				base: 'NS',
				platform: 'android'
			},
			ios:{
				base: 'NS',
				platform: 'ios'
			},
			ios_simulator:{
				base: 'NS',
				platform: 'ios',
				arguments: ['--emulator']
			}
		},

		singleRun: false
	})
}